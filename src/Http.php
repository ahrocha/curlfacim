<?php

namespace Ideal\Neo\CurlFacim;

/**
 * Requests curl http
 *
 * @author andrey.rocha@idealinvest.com.br
 */
class Http
{
    
    public static function post($endpoint, $headers, $fields)
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $endpoint);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($fields));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        
        if (!is_null($headers)) {
            foreach ($headers as $header) {
                curl_setopt($ch, CURLOPT_HTTPHEADER, [$header]);
            }
        }

        $server_output = curl_exec($ch);

        curl_close($ch);

        return ( $server_output );
    }


    public static function get($endpoint, array $headers)
    {
        
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $endpoint);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        if (!is_null($headers)) {
            foreach ($headers as $header) {
                curl_setopt($ch, CURLOPT_HTTPHEADER, [$header]);
            }
        }
        $server_output = curl_exec($ch);
        curl_close($ch);
        return ( $server_output );
    }

    public static function json($endpoint, $headers, array $fields)
    {
        $fields = json_encode($fields);
        
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $endpoint);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        
        if (!is_null($headers)) {
            foreach ($headers as $header) {
                curl_setopt($ch, CURLOPT_HTTPHEADER, [$header]);
            }
        }

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($fields)));

        $server_output = curl_exec($ch);
        curl_close($ch);

        return ( $server_output );
    }
}
